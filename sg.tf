resource "aws_security_group" "bootcamp13" {
  name        = "bootcamp13"
  description = "Security group for web-server with HTTP ports open within VPC"
  vpc_id      = aws_vpc.bootcamp13.id
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
