#Create a VPC ressource#
resource "aws_vpc" "bootcamp13" {
  cidr_block       = "10.2.0.0/16"
  instance_tenancy = "default"
  #  tags             = merge(local.aws_tags, { Name = "vpc-bootcamp13" }) 
}

#Create an internet gateway ressource#
resource "aws_internet_gateway" "bootcamp13" {
  vpc_id = aws_vpc.bootcamp13.id
  #  tags   = merge(local.aws_tags, { Name = "gateway-bootcamp13" })
}

#Create a route table ressource#
resource "aws_route_table" "bootcamp13" {
  vpc_id = aws_vpc.bootcamp13.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.bootcamp13.id
  }
  #      tags       = merge(local.aws_tags, { Name = "route-table-bootcamp13" })
}

#Create a route table association ressource#
resource "aws_route_table_association" "bootcamp13" {
  subnet_id      = aws_subnet.bootcamp13.id
  route_table_id = aws_route_table.bootcamp13.id
}

#Create a VPC subnet ressource#
resource "aws_subnet" "bootcamp13" {
  vpc_id            = aws_vpc.bootcamp13.id
  cidr_block        = "10.2.1.0/24"
  availability_zone = "eu-west-2a"
  #  tags              = merge(local.aws_tags, { Name = "subnet-bootcamp13" })
}
